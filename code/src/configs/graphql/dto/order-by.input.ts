import { Field, InputType, registerEnumType } from '@nestjs/graphql';

export enum OrderEnum {
  ASC = 'asc',
  DESC = 'desc',
}

registerEnumType(OrderEnum, { name: 'OrderEnum' });

export function CreateOrderByInput(fields: string[], name: string) {
  const fieldEnum = arrayToEnum(fields);

  registerEnumType(fieldEnum, { name: `OrderByFieldEnum${name}` });

  @InputType()
  abstract class OrderByInput {
    @Field(() => fieldEnum, { nullable: true }) orderField?: string;
    @Field(() => OrderEnum, { nullable: true }) orderType?: OrderEnum;
  }

  return OrderByInput;
}

function arrayToEnum(arr: string[]): Record<string, string> {
  const enumObject: Record<string, string> = {} as Record<string, string>;
  arr.forEach(item => {
    enumObject[item.toUpperCase()] = item;
  });
  return enumObject;
}
