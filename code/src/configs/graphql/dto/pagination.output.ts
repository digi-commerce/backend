import { ObjectType, Field, Int, PickType, InputType } from '@nestjs/graphql';

interface IPagination {
  page: number;
  pageSize: number;
  total: number;
  totalPage: number;
}

@ObjectType()
export class PaginationOutput implements IPagination {
  @Field(() => Int, { nullable: true }) page: number;
  @Field(() => Int, { nullable: true }) pageSize: number;
  @Field(() => Int, { nullable: true }) total: number;
  @Field(() => Int, { nullable: true }) totalPage: number;
}

@InputType()
export class PaginationInput extends PickType(PaginationOutput, [
  'page',
  'pageSize',
]) {}
