import { Field, ObjectType } from '@nestjs/graphql';
import { PaginationOutput } from './pagination.output';

@ObjectType()
export class MetaOutput {
  @Field({ nullable: true }) pagination?: PaginationOutput;
  @Field({ nullable: true })
  apiVersion?: string;
  @Field({ nullable: true }) timeResponse?: number;
}
