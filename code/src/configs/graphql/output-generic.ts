import { Field, ObjectType } from '@nestjs/graphql';
import { MetaOutput } from './dto/meta-data.output';

export default function OutputGeneric<TItem extends object>(
  TItemClass: new (...args: any[]) => TItem,
  type: 'object' | 'array',
) {
  @ObjectType({ isAbstract: true })
  abstract class OutputGenericClass {
    @Field(() => (type === 'object' ? TItemClass : [TItemClass]))
    data: TItem[] | TItem;

    @Field(() => MetaOutput, { nullable: true })
    meta?: MetaOutput;
  }
  return OutputGenericClass;
}
