import { Field, InputType } from '@nestjs/graphql';
import { IsUUID } from 'class-validator';

@InputType()
export class FindOneInput {
  @IsUUID(4) @Field(() => String) id: string;
}
