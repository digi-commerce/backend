import { ObjectType } from '@nestjs/graphql';
import OutputGeneric from 'src/configs/graphql/output-generic';
import { User } from '../entities/user.entity';

@ObjectType()
export class UserOutput extends OutputGeneric(User, 'object') {}

@ObjectType()
export class UsersOutput extends OutputGeneric(User, 'array') {}
