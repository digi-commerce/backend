import { Field, InputType, Int } from '@nestjs/graphql';
import { Prisma, ROLE } from '@prisma/client';

@InputType()
export class UpdateUserInput implements Prisma.UserUpdateInput {
  @Field(() => String, { nullable: false }) id: string;
  @Field(() => String, { nullable: true }) firstName?: string;
  @Field(() => String, { nullable: true }) lastName?: string;
  @Field(() => String, { nullable: true }) phone?: string;
  @Field(() => String, { nullable: true }) avatar?: string;
  @Field(() => String, { nullable: true }) document?: string;
  @Field(() => Date, { nullable: true }) dateOfBirth?: Date;
  @Field(() => ROLE, { nullable: true }) role?: ROLE;
  @Field(() => Boolean, { nullable: true }) active?: boolean;
}
