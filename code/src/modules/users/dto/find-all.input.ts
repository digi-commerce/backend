import { Field, InputType, Int } from '@nestjs/graphql';
import { CreateOrderByInput } from 'src/configs/graphql/dto/order-by.input';

@InputType()
export class FindAllUserInput extends CreateOrderByInput(
  ['document', 'firstName', 'lastName', 'updatedAt', 'createdAt'],
  'FindAllUserInput',
) {
  @Field(() => String, { nullable: true }) id?: string;
  @Field(() => String, { nullable: true }) document?: string;
  @Field(() => String, { nullable: true }) firstName?: string;
  @Field(() => String, { nullable: true }) lastName?: string;
  @Field(() => Boolean, { nullable: true }) active?: boolean;
  @Field(() => Int, { nullable: true }) pageSize?: number;
  @Field(() => Int, { nullable: true }) pageNumber?: number;
}
