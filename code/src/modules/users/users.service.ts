import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { PaginationOutput } from 'src/configs/graphql/dto/pagination.output';
import { genPassword } from 'src/core/utils/bcrypt';
import { PrismaService } from '../../services/prisma/prisma.service';
import { FindAllUserInput } from './dto/find-all.input';
import { UpdateUserInput } from './dto/update-user.input';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(private readonly prisma: PrismaService) {}

  async create(createUserInput: Prisma.UserCreateInput) {
    createUserInput.password = genPassword(createUserInput.password);
    return await this.prisma.user.create({
      data: createUserInput,
      select: {
        id: true,
        email: true,
        firstName: true,
        createdAt: true,
        updatedAt: true,
        lastName: true,
        phone: true,
        avatar: true,
        dateOfBirth: true,
        active: true,
        role: true,
      },
    });
  }

  async findAll(
    findAllInput: FindAllUserInput,
  ): Promise<{ data: User[]; pagination: PaginationOutput }> {
    const pageSize = findAllInput.pageSize ? findAllInput.pageSize : 5;
    const pageNumber = findAllInput.pageNumber ? findAllInput.pageNumber : 1;
    const offset = pageSize * (pageNumber - 1);

    const conditions: Prisma.UserWhereInput = {
      ...(findAllInput.firstName && {
        firstName: { contains: findAllInput.firstName },
      }),
      ...(findAllInput.lastName && {
        lastName: { contains: findAllInput.lastName },
      }),
      ...(findAllInput.active && { active: findAllInput.active }),
      ...(findAllInput.document && {
        document: { contains: findAllInput.document },
      }),
      ...(findAllInput.id && { id: findAllInput.id }),
    };
    const [total, items] = (await this.prisma.$transaction([
      this.prisma.user.count({
        where: conditions,
      }),
      this.prisma.user.findMany({
        where: conditions,
        take: pageSize,
        skip: offset,
        orderBy: {
          ...(findAllInput.orderField
            ? {
                [findAllInput.orderField]: findAllInput.orderType,
              }
            : { createdAt: 'desc' }),
        },
        select: {
          id: true,
          email: true,
          firstName: true,
          createdAt: true,
          updatedAt: true,
          lastName: true,
          phone: true,
          avatar: true,
          dateOfBirth: true,
          active: true,
          role: true,
        },
      }),
    ])) ?? [0, []];

    const totalPage = Math.ceil(total / pageSize);

    return {
      data: items,
      pagination: { page: pageNumber, total, totalPage, pageSize },
    };
  }

  async findOne(id: string) {
    return await this.prisma.user.findUnique({
      where: { id },
      select: {
        id: true,
        email: true,
        firstName: true,
        createdAt: true,
        updatedAt: true,
        lastName: true,
        phone: true,
        avatar: true,
        dateOfBirth: true,
        active: true,
        role: true,
      },
    });
  }

  async findOneByEmail(email: string) {
    return await this.prisma.user.findFirst({ where: { email } });
  }

  async update(updateUserInput: UpdateUserInput) {
    return await this.prisma.user.update({
      data: updateUserInput,
      where: {
        id: updateUserInput.id,
      },
      select: {
        id: true,
        email: true,
        firstName: true,
        createdAt: true,
        updatedAt: true,
        lastName: true,
        phone: true,
        avatar: true,
        dateOfBirth: true,
        active: true,
        role: true,
      },
    });
  }

  async remove(id: string) {
    return await this.prisma.user.delete({ where: { id: id } });
  }
}
