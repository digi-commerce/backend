import { Field, ObjectType, registerEnumType } from '@nestjs/graphql';
import { ROLE, User as PrismaUser } from '@prisma/client';

registerEnumType(ROLE, { name: 'Role' });

@ObjectType()
export class User {
  @Field(() => String, { nullable: true }) id: string;
  @Field(() => String, { nullable: true }) email: string;
  @Field(() => ROLE, { nullable: true }) role: ROLE;
  @Field(() => Date, { nullable: true }) createdAt: Date;
  @Field(() => Date, { nullable: true }) updatedAt: Date;
  @Field(() => String, { nullable: true }) firstName: string;
  @Field(() => String, { nullable: true }) lastName: string | null;
  @Field(() => String, { nullable: true }) phone: string | null;
  @Field(() => String, { nullable: true }) avatar: string | null;
  @Field(() => Date, { nullable: true }) dateOfBirth: Date | null;
  @Field(() => Boolean, { nullable: true }) active: boolean;
  password?: string;
  confirmEmail?: string | null;
  resetPassword?: string | null;
}
