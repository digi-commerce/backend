import { NotFoundException, UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { CurrentToken } from 'src/core/decorators/CurrentTokenGql.decorator';
import { Public } from 'src/core/decorators/public.decorator';
import { Roles } from 'src/core/decorators/roles.decorator';
import { GqlAuthGuard } from 'src/guards/gql-auth/gql-auth.guard';
import { RoleGuard } from 'src/guards/role/role.guard';
import { ExceptionsHandler } from '../../core/exceptions/exceptions-handler';
import { CreateUserInput } from './dto/create-user.input';
import { FindAllUserInput } from './dto/find-all.input';
import { FindOneInput } from './dto/find-one.input';
import { UpdateUserInput } from './dto/update-user.input';
import { UserOutput, UsersOutput } from './dto/user.output';
import { User } from './entities/user.entity';
import { UsersService } from './users.service';

@UseGuards(GqlAuthGuard, RoleGuard)
@Resolver(() => User)
export class UsersResolver extends ExceptionsHandler {
  constructor(private readonly usersService: UsersService) {
    super();
  }

  @Public()
  @Mutation(() => UserOutput)
  async createUser(@Args('createUserInput') createUserInput: CreateUserInput) {
    try {
      const user = await this.usersService.create(createUserInput);
      return { data: user };
    } catch (error) {
      this.handleError(error);
    }
  }

  @Roles('MANAGER')
  @Query(() => UsersOutput, { name: 'users', nullable: true })
  async findAll(
    @Args('findAllInput', { nullable: true }) findAllInput?: FindAllUserInput,
  ): Promise<UsersOutput | undefined> {
    try {
      const users = await this.usersService.findAll(findAllInput || {});
      return {
        data: users.data,
        meta: { pagination: users.pagination },
      };
    } catch (error) {
      this.handleError(error);
    }
  }

  @Roles('MANAGER', 'ORDER', 'USER')
  @Query(() => UserOutput, { name: 'user', nullable: true })
  async findOne(
    @Args('findOneInput') findOneInput: FindOneInput,
    @CurrentToken() context: User,
  ): Promise<UserOutput | undefined> {
    try {
      if ('MANAGER' !== context.role) findOneInput.id = context.id;
      const user = await this.usersService.findOne(findOneInput.id);
      if (!user)
        throw new NotFoundException(`User ${findOneInput.id} not found`);
      return { data: user };
    } catch (error) {
      this.handleError(error);
    }
  }

  @Roles('MANAGER', 'ORDER', 'USER')
  @Mutation(() => UserOutput, { name: 'updateUser', nullable: true })
  async updateUser(
    @Args('updateUserInput') updateUserInput: UpdateUserInput,
    @CurrentToken() context: User,
  ): Promise<UserOutput | undefined> {
    try {
      if ('MANAGER' !== context.role) {
        updateUserInput.id = context.id;
        delete updateUserInput.role;
        delete updateUserInput.active;
      }

      const user = await this.usersService.update(updateUserInput);
      return { data: user };
    } catch (error) {
      this.handleError(error);
    }
  }

  @Roles('MANAGER', 'ORDER', 'USER')
  @Mutation(() => UserOutput, { name: 'removeUser', nullable: true })
  async removeUser(
    @Args('findOneInput') findOneInput: FindOneInput,
    @CurrentToken() context: User,
  ): Promise<UserOutput | undefined> {
    try {
      if ('MANAGER' !== context.role) findOneInput.id = context.id;
      const user = await this.usersService.remove(findOneInput.id);
      if (!user)
        throw new NotFoundException(`User ${findOneInput.id} not found`);
      return { data: user };
    } catch (error) {
      this.handleError(error);
    }
  }
}
