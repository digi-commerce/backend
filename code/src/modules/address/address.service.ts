import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/services/prisma/prisma.service';
import { CreateAddressInput } from './dto/create-address.input';
import { FindAllAddressInput } from './dto/find-all-address.input';
import { UpdateAddressInput } from './dto/update-address.input';
import { Prisma } from '@prisma/client';

@Injectable()
export class AddressService {
  constructor(private readonly prisma: PrismaService) {}

  async create(createAddressInput: CreateAddressInput) {
    return await this.prisma.address.create({
      data: createAddressInput,
    });
  }

  async findAll(findAllInput: FindAllAddressInput) {
    const pageSize = findAllInput.pageSize ? findAllInput.pageSize : 5;
    const pageNumber = findAllInput.pageNumber ? findAllInput.pageNumber : 1;
    const offset = pageSize * (pageNumber - 1);

    const conditions: Prisma.ProductWhereInput = {
      ...(findAllInput.userId && { userId: findAllInput.userId }),
      ...(findAllInput.id && { id: findAllInput.id }),
    };
    const [total, items] = (await this.prisma.$transaction([
      this.prisma.address.count({
        where: conditions,
      }),
      this.prisma.address.findMany({
        where: conditions,
        take: pageSize,
        skip: offset,
        orderBy: {
          ...(findAllInput.orderField
            ? {
                [findAllInput.orderField]: findAllInput.orderType,
              }
            : { createdAt: 'desc' }),
        },
      }),
    ])) ?? [0, []];

    const totalPage = Math.ceil(total / pageSize);

    return {
      data: items,
      pagination: { page: pageNumber, total, totalPage, pageSize },
    };
  }

  async findOne(id: string) {
    return await this.prisma.address.findUnique({
      where: { id },
    });
  }

  async update(updateAddressInput: UpdateAddressInput) {
    return await this.prisma.address.update({
      where: { id: updateAddressInput.id },
      data: updateAddressInput,
    });
  }

  async remove(id: string) {
    return await this.prisma.address.delete({
      where: { id },
    });
  }
}
