import { Field, InputType, Int } from '@nestjs/graphql';
import { Prisma } from '@prisma/client';
import {
  IsInt,
  IsNotEmpty,
  IsNumberString,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';

@InputType()
export class UpdateAddressInput implements Prisma.AddressUncheckedUpdateInput {
  @IsNotEmpty()
  @IsUUID(4)
  @Field(() => String)
  id: string;

  @IsOptional()
  @IsNumberString()
  @Field(() => String, { nullable: true })
  cep?: string;

  @IsOptional()
  @IsInt()
  @Field(() => Int, { nullable: true })
  number?: number;

  @IsOptional()
  @IsString()
  @Field(() => String, { nullable: true })
  complement?: string;

  @IsOptional()
  @IsString()
  @Field(() => String, { nullable: true })
  address?: string;

  @IsOptional()
  @IsString()
  @Field(() => String, { nullable: true })
  neighborhood?: string;

  @IsOptional()
  @IsInt()
  @Field(() => Int)
  cityId?: number;

  @IsOptional()
  @IsString()
  @Field(() => String, { nullable: true })
  title?: string;
}
