import { Field, InputType, Int } from '@nestjs/graphql';
import { IsNumber, IsOptional, IsString, IsUUID } from 'class-validator';
import { CreateOrderByInput } from 'src/configs/graphql/dto/order-by.input';

@InputType()
export class FindAllAddressInput extends CreateOrderByInput(
  ['updatedAt', 'createdAt'],
  'FindAllAdressInput',
) {
  @IsOptional() @IsUUID(4) @Field(() => String, { nullable: true }) id?: string;

  @IsOptional()
  @IsUUID(4)
  @Field(() => String, { nullable: true })
  userId?: string;

  @IsOptional()
  @IsNumber()
  @Field(() => Int, { nullable: true })
  pageSize?: number;

  @IsOptional()
  @IsNumber()
  @Field(() => Int, { nullable: true })
  pageNumber?: number;
}
