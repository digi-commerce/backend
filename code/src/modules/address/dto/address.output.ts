import { ObjectType } from '@nestjs/graphql';
import OutputGeneric from 'src/configs/graphql/output-generic';
import { Address } from '../entities/address.entity';

@ObjectType()
export class AddressOutput extends OutputGeneric(Address, 'object') {}

@ObjectType()
export class AddressesOutput extends OutputGeneric(Address, 'array') {}
