import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { ROLE } from '@prisma/client';
import { CurrentToken } from 'src/core/decorators/CurrentTokenGql.decorator';
import { AddressService } from './address.service';
import { CreateAddressInput } from './dto/create-address.input';
import { FindAllAddressInput } from './dto/find-all-address.input';
import { FindOneAddressInput } from './dto/find-one-address.input';
import { UpdateAddressInput } from './dto/update-address.input';
import { Address } from './entities/address.entity';
import { AddressOutput, AddressesOutput } from './dto/address.output';
import { NotFoundException, UseGuards } from '@nestjs/common';
import { GqlAuthGuard } from 'src/guards/gql-auth/gql-auth.guard';
import { RoleGuard } from 'src/guards/role/role.guard';
import { ExceptionsHandler } from 'src/core/exceptions/exceptions-handler';
import { Roles } from 'src/core/decorators/roles.decorator';
import { User } from '../users/entities/user.entity';

@UseGuards(GqlAuthGuard, RoleGuard)
@Resolver(() => Address)
export class AddressResolver extends ExceptionsHandler {
  constructor(private readonly addressService: AddressService) {
    super();
  }

  @Roles('ORDER', 'USER')
  @Mutation(() => AddressOutput)
  async createAddress(
    @Args('createAddressInput') createAddressInput: CreateAddressInput,
  ): Promise<AddressOutput | undefined> {
    try {
      const address = await this.addressService.create(createAddressInput);
      return { data: address };
    } catch (error) {
      this.handleError(error);
    }
  }

  @Roles('ORDER')
  @Query(() => AddressesOutput, { name: 'addresses' })
  async findAll(
    @Args('findAllAddressInput', { nullable: true })
    findAllAddressInput: FindAllAddressInput,
  ): Promise<AddressesOutput | undefined> {
    try {
      const result = await this.addressService.findAll(findAllAddressInput);
      return { data: result.data, meta: { pagination: result.pagination } };
    } catch (error) {
      this.handleError(error);
    }
  }

  @Roles('ORDER', 'USER')
  @Query(() => AddressOutput, { name: 'address' })
  async findOne(
    @Args('findOneAddressInput') findOneAddressInput: FindOneAddressInput,
  ): Promise<AddressOutput | undefined> {
    try {
      const address = await this.addressService.findOne(findOneAddressInput.id);
      if (!address)
        throw new NotFoundException(
          `Address ${findOneAddressInput.id} not found`,
        );
      return { data: address };
    } catch (error) {
      this.handleError(error);
    }
  }

  @Roles('ORDER', 'USER')
  @Mutation(() => AddressOutput)
  async updateAddress(
    @Args('updateAddressInput') updateAddressInput: UpdateAddressInput,
    @CurrentToken() user: User,
  ): Promise<AddressOutput | undefined> {
    try {
      const id = ROLE.MANAGER === user.role ? updateAddressInput.id : user.id;
      const address = await this.addressService.update({
        ...updateAddressInput,
        id: id,
      });
      return { data: address };
    } catch (error) {
      this.handleError(error);
    }
  }

  @Roles('ORDER', 'USER')
  @Mutation(() => AddressOutput)
  async removeAddress(
    @Args('deleteAddressInput') findOneAddressInput: FindOneAddressInput,
    @CurrentToken() user: User,
  ): Promise<AddressOutput | undefined> {
    try {
      const id = ROLE.MANAGER === user.role ? findOneAddressInput.id : user.id;
      const address = await this.addressService.remove(id);
      if (!address)
        throw new NotFoundException(
          `Address ${findOneAddressInput.id} not found`,
        );
      return { data: address };
    } catch (error) {
      this.handleError(error);
    }
  }
}
