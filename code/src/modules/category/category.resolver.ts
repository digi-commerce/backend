import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { ExceptionsHandler } from 'src/core/exceptions/exceptions-handler';
import { CategoryService } from './category.service';
import { CategoriesOutput, CategoryOutput } from './dto/category.output';
import { CreateCategoryInput } from './dto/create-category.input';
import { FindAllCategoryInput } from './dto/find-all-category.input';
import { FindOneCategoryInput } from './dto/find-one-category.input';
import { UpdateCategoryInput } from './dto/update-category.input';
import { Category } from './entities/category.entity';
import { NotFoundException, UseGuards } from '@nestjs/common';
import { GqlAuthGuard } from 'src/guards/gql-auth/gql-auth.guard';
import { RoleGuard } from 'src/guards/role/role.guard';
import { Roles } from 'src/core/decorators/roles.decorator';
import { Public } from 'src/core/decorators/public.decorator';

@UseGuards(GqlAuthGuard, RoleGuard)
@Resolver(() => Category)
export class CategoryResolver extends ExceptionsHandler {
  constructor(private readonly categoryService: CategoryService) {
    super();
  }

  @Roles('MANAGER', 'ORDER')
  @Mutation(() => CategoryOutput)
  async createCategory(
    @Args('createCategoryInput') createCategoryInput: CreateCategoryInput,
  ): Promise<CategoryOutput | undefined> {
    try {
      const category = await this.categoryService.create(createCategoryInput);
      return { data: category };
    } catch (error) {
      this.handleError(error);
    }
  }

  @Public()
  @Query(() => CategoryOutput, { name: 'categories' })
  async findAll(
    @Args('findAllCategoryInput') findAllCategoryInput: FindAllCategoryInput,
  ): Promise<CategoriesOutput | undefined> {
    try {
      const result = await this.categoryService.findAll(findAllCategoryInput);
      return { data: result.data, meta: { pagination: result.pagination } };
    } catch (error) {
      this.handleError(error);
    }
  }

  @Public()
  @Query(() => CategoryOutput, { name: 'category' })
  async findOne(
    @Args('findOneCategoryInput') findOneCategoryInput: FindOneCategoryInput,
  ): Promise<CategoryOutput | undefined> {
    try {
      const category = await this.categoryService.findOne(findOneCategoryInput);
      if (!category)
        throw new NotFoundException(
          `Category ${findOneCategoryInput.id} not found`,
        );
      return { data: category };
    } catch (error) {
      this.handleError(error);
    }
  }

  @Roles('MANAGER', 'ORDER')
  @Mutation(() => CategoryOutput)
  async updateCategory(
    @Args('updateCategoryInput') updateCategoryInput: UpdateCategoryInput,
  ): Promise<CategoryOutput | undefined> {
    try {
      const category = await this.categoryService.update(updateCategoryInput);
      return { data: category };
    } catch (error) {
      this.handleError(error);
    }
  }

  @Roles('MANAGER', 'ORDER')
  @Mutation(() => CategoryOutput)
  async removeCategory(
    @Args('findOneCategoryInput') findOneCategoryInput: FindOneCategoryInput,
  ): Promise<CategoryOutput | undefined> {
    try {
      const category = await this.categoryService.remove(
        findOneCategoryInput.id,
      );
      if (!category)
        throw new NotFoundException(
          `Category ${findOneCategoryInput.id} not found`,
        );
      return { data: category };
    } catch (error) {
      this.handleError(error);
    }
  }
}
