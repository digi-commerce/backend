import { Field, InputType, Int } from '@nestjs/graphql';
import { Type } from 'class-transformer';
import {
  IsDate,
  IsNumber,
  IsOptional,
  IsUUID,
  IsString,
} from 'class-validator';
import { CreateOrderByInput } from 'src/configs/graphql/dto/order-by.input';

@InputType()
export class FindAllCategoryInput extends CreateOrderByInput(
  ['name', 'updatedAt', 'createdAt'],
  'FindAllCategoryInput',
) {
  @IsOptional() @IsUUID(4) @Field(() => String, { nullable: true }) id?: string;

  @IsOptional()
  @IsString()
  @Field(() => String, { nullable: true })
  name?: string;

  @IsOptional()
  @IsNumber()
  @Field(() => Int, { nullable: true })
  pageSize?: number;

  @IsOptional()
  @IsNumber()
  @Field(() => Int, { nullable: true })
  pageNumber?: number;
}
