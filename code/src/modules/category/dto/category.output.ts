import { ObjectType } from '@nestjs/graphql';
import OutputGeneric from 'src/configs/graphql/output-generic';
import { Category } from '../entities/category.entity';

@ObjectType()
export class CategoryOutput extends OutputGeneric(Category, 'object') {}

@ObjectType()
export class CategoriesOutput extends OutputGeneric(Category, 'array') {}
