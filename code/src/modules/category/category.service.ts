import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { PrismaService } from 'src/services/prisma/prisma.service';
import { FindAllCategoryInput } from './dto/find-all-category.input';
import { FindOneCategoryInput } from './dto/find-one-category.input';
import { UpdateCategoryInput } from './dto/update-category.input';

@Injectable()
export class CategoryService {
  constructor(private readonly prisma: PrismaService) {}

  async create(createCategoryInput: Prisma.CategoryCreateInput) {
    return await this.prisma.category.create({
      data: createCategoryInput,
    });
  }

  async findAll(findAllInput: FindAllCategoryInput) {
    const pageSize = findAllInput.pageSize ? findAllInput.pageSize : 5;
    const pageNumber = findAllInput.pageNumber ? findAllInput.pageNumber : 1;
    const offset = pageSize * (pageNumber - 1);

    const conditions: Prisma.ProductWhereInput = {
      ...(findAllInput.name && {
        name: { contains: findAllInput.name },
      }),
      ...(findAllInput.id && { id: findAllInput.id }),
    };
    const [total, items] = (await this.prisma.$transaction([
      this.prisma.category.count({
        where: conditions,
      }),
      this.prisma.category.findMany({
        where: conditions,
        take: pageSize,
        skip: offset,
        orderBy: {
          ...(findAllInput.orderField
            ? {
                [findAllInput.orderField]: findAllInput.orderType,
              }
            : { createdAt: 'desc' }),
        },
      }),
    ])) ?? [0, []];

    const totalPage = Math.ceil(total / pageSize);

    return {
      data: items,
      pagination: { page: pageNumber, total, totalPage, pageSize },
    };
  }

  async findOne(findOneCategoryInput: FindOneCategoryInput) {
    return await this.prisma.category.findUnique({
      where: { id: findOneCategoryInput.id },
    });
  }

  async update(updateCategoryInput: UpdateCategoryInput) {
    return await this.prisma.category.update({
      where: { id: updateCategoryInput.id },
      data: updateCategoryInput,
    });
  }

  async remove(id: string) {
    return await this.prisma.category.delete({
      where: { id },
    });
  }
}
