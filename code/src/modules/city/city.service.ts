import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/services/prisma/prisma.service';
import { FindAllCityInput } from './dto/find-all-city.input';
import { FindOneCityInput } from './dto/find-one-city.input';
import { Prisma } from '@prisma/client';

@Injectable()
export class CityService {
  constructor(private readonly prisma: PrismaService) {}

  async findAll(findAllInput: FindAllCityInput) {
    const pageSize = findAllInput.pageSize ? findAllInput.pageSize : 5;
    const pageNumber = findAllInput.pageNumber ? findAllInput.pageNumber : 1;
    const offset = pageSize * (pageNumber - 1);

    const conditions: Prisma.CityWhereInput = {
      ...(findAllInput.name && {
        name: { contains: findAllInput.name },
      }),
      ...(findAllInput.stateId && { stateId: findAllInput.stateId }),
      ...(findAllInput.id && { id: findAllInput.id }),
    };
    const [total, items] = (await this.prisma.$transaction([
      this.prisma.city.count({
        where: conditions,
      }),
      this.prisma.city.findMany({
        where: conditions,
        take: pageSize,
        skip: offset,
        orderBy: {
          ...(findAllInput.orderField
            ? {
                [findAllInput.orderField]: findAllInput.orderType,
              }
            : { createdAt: 'desc' }),
        },
      }),
    ])) ?? [0, []];

    const totalPage = Math.ceil(total / pageSize);

    return {
      data: items,
      pagination: { page: pageNumber, total, totalPage, pageSize },
    };
  }

  async findOne(findOneCityInput: FindOneCityInput) {
    return await this.prisma.city.findFirst({
      where: { id: findOneCityInput.id },
    });
  }

  async findAllState() {
    const [total, items] = (await this.prisma.$transaction([
      this.prisma.state.count(),
      this.prisma.state.findMany({
        orderBy: { name: 'asc' },
      }),
    ])) ?? [0, []];

    return {
      data: items,
      pagination: { page: 1, total, totalPage: 1, pageSize: total },
    };
  }
}
