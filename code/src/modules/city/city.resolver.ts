import { NotFoundException } from '@nestjs/common';
import { Args, Query, Resolver } from '@nestjs/graphql';
import { ExceptionsHandler } from 'src/core/exceptions/exceptions-handler';
import { CityService } from './city.service';
import { CitiesOutput, CityOutput, StatesOutput } from './dto/city.output';
import { FindAllCityInput } from './dto/find-all-city.input';
import { FindOneCityInput } from './dto/find-one-city.input';
import { City } from './entities/city.entity';

@Resolver(() => City)
export class CityResolver extends ExceptionsHandler {
  constructor(private readonly cityService: CityService) {
    super();
  }

  @Query(() => CitiesOutput, { name: 'cities' })
  async findAll(
    @Args('findAllCityInput') findAllCityInput: FindAllCityInput,
  ): Promise<CitiesOutput | undefined> {
    try {
      const result = await this.cityService.findAll(findAllCityInput);
      return { data: result.data, meta: { pagination: result.pagination } };
    } catch (error) {
      this.handleError(error);
    }
  }

  @Query(() => CityOutput, { name: 'city' })
  async findOne(
    @Args('FindOneCityInput') findOneCityInput: FindOneCityInput,
  ): Promise<CityOutput | null | undefined> {
    try {
      const city = await this.cityService.findOne(findOneCityInput);
      if (!city)
        throw new NotFoundException(`Product ${findOneCityInput.id} not found`);
      return { data: city };
    } catch (error) {
      this.handleError(error);
    }
  }

  @Query(() => StatesOutput, { name: 'states' })
  async findAllState(): Promise<StatesOutput | null | undefined> {
    try {
      const result = await this.cityService.findAllState();
      return { data: result.data, meta: { pagination: result.pagination } };
    } catch (error) {
      this.handleError(error);
    }
  }
}
