import { Field, InputType, Int } from '@nestjs/graphql';
import { IsInt, IsNumber, IsOptional, IsString, IsUUID } from 'class-validator';
import { CreateOrderByInput } from 'src/configs/graphql/dto/order-by.input';

@InputType()
export class FindAllCityInput extends CreateOrderByInput(
  ['name', 'stateId', 'updatedAt', 'createdAt'],
  'FindAllCityInput',
) {
  @IsOptional() @IsInt() @Field(() => Int, { nullable: true }) id?: number;

  @IsOptional()
  @IsString()
  @Field(() => String, { nullable: true })
  name?: string;

  @IsOptional()
  @IsNumber()
  @Field(() => Int, { nullable: true })
  stateId?: number;

  @IsOptional()
  @IsNumber()
  @Field(() => Int, { nullable: true })
  pageSize?: number;

  @IsOptional()
  @IsNumber()
  @Field(() => Int, { nullable: true })
  pageNumber?: number;
}
