import { Field, InputType, Int } from '@nestjs/graphql';
import { IsInt, IsOptional, IsUUID } from 'class-validator';

@InputType()
export class FindOneCityInput {
  @IsOptional() @IsInt() @Field(() => Int) id?: number;
}
