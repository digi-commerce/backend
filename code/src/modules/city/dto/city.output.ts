import { ObjectType } from '@nestjs/graphql';
import OutputGeneric from 'src/configs/graphql/output-generic';
import { City } from '../entities/city.entity';
import { State } from '../entities/state.entity';

@ObjectType()
export class CityOutput extends OutputGeneric(City, 'object') {}

@ObjectType()
export class CitiesOutput extends OutputGeneric(City, 'array') {}

@ObjectType()
export class StatesOutput extends OutputGeneric(State, 'array') {}
