import { Field, ObjectType } from '@nestjs/graphql';
import { ROLE } from '@prisma/client';

@ObjectType()
export class LoginOutput {
  @Field(() => String) token: string;
  @Field(() => ROLE) role: ROLE;
}
