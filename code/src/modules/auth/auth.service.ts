import { Injectable, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { compare } from 'src/core/utils/bcrypt';
import { User } from '../users/entities/user.entity';
import { UsersService } from '../users/users.service';
import { IPayload } from './dto/IPayload';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async validateCredentials(email: string, password_: string): Promise<any> {
    const user = await this.userService.findOneByEmail(email);
    console.log(user);

    if (!user) return null;
    console.log('s: ' + compare(password_, user.password));
    if (!compare(password_, user.password)) return null;
    if (!user.active) return null;

    const { password, ...result } = user;
    return result;
  }

  async validateJwt(payload: IPayload): Promise<any> {
    const user = await this.userService.findOne(payload.id);

    if (!user) return null;
    if (!user.active) return null;

    return user;
  }

  async login(user: User): Promise<any> {
    const payload = { id: user.id };
    const access_token = this.jwtService.sign(payload);

    return { token: access_token };
  }

  async logout({ id: id }): Promise<boolean> {
    const user = await this.userService.findOne(id);
    if (!user) throw new NotFoundException();

    return true;
  }
}
