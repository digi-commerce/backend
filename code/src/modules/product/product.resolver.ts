import { NotFoundException, UseGuards } from '@nestjs/common';
import { Args, Int, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Roles } from 'src/core/decorators/roles.decorator';
import { ExceptionsHandler } from 'src/core/exceptions/exceptions-handler';
import { GqlAuthGuard } from 'src/guards/gql-auth/gql-auth.guard';
import { RoleGuard } from 'src/guards/role/role.guard';
import { CreateProductInput } from './dto/create-product.input';
import { FindAllProductInput } from './dto/find-all-product.input';
import { ProductOutput, ProductsOutput } from './dto/product.output';
import { UpdateProductInput } from './dto/update-product.input';
import { ProductService } from './product.service';
import { FindOneProductInput } from './dto/find-one-product.input';

@UseGuards(GqlAuthGuard, RoleGuard)
@Resolver(() => ProductOutput)
export class ProductResolver extends ExceptionsHandler {
  constructor(private readonly productService: ProductService) {
    super();
  }

  @Roles('MANAGER', 'ORDER')
  @Mutation(() => ProductOutput)
  async createProduct(
    @Args('createProductInput') createProductInput: CreateProductInput,
  ): Promise<ProductOutput | undefined> {
    try {
      const product = await this.productService.create(createProductInput);
      return { data: product };
    } catch (error: any) {
      this.handleError(error);
    }
  }

  @Roles('MANAGER', 'ORDER', 'USER')
  @Query(() => ProductsOutput, { name: 'products' })
  async findAll(
    @Args('findAllInput', { nullable: true })
    findAllInput?: FindAllProductInput,
  ): Promise<ProductsOutput | undefined> {
    try {
      const product = await this.productService.findAll(findAllInput || {});
      return { data: product.data, meta: { pagination: product.pagination } };
    } catch (error: any) {
      this.handleError(error);
    }
  }

  @Roles('MANAGER', 'ORDER', 'USER')
  @Query(() => ProductOutput, { name: 'product' })
  async findOne(
    @Args('findOneProductInput') findOneProductInput: FindOneProductInput,
  ): Promise<ProductOutput | undefined> {
    try {
      const product = await this.productService.findOne(findOneProductInput.id);
      if (!product)
        throw new NotFoundException(
          `Product ${findOneProductInput.id} not found`,
        );
      return { data: product };
    } catch (error: any) {
      this.handleError(error);
    }
  }

  @Roles('MANAGER', 'ORDER')
  @Mutation(() => ProductOutput)
  async updateProduct(
    @Args('updateProductInput') updateProductInput: UpdateProductInput,
  ): Promise<ProductOutput | undefined> {
    try {
      const product = await this.productService.update(
        updateProductInput.id,
        updateProductInput,
      );
      return { data: product };
    } catch (error: any) {
      this.handleError(error);
    }
  }

  @Roles('MANAGER')
  @Mutation(() => ProductOutput)
  async removeProduct(
    @Args('findOneProductInput') findOneProductInput: FindOneProductInput,
  ): Promise<ProductOutput | undefined> {
    try {
      const product = await this.productService.remove(findOneProductInput.id);
      if (!product)
        throw new NotFoundException(
          `Product ${findOneProductInput.id} not found`,
        );
      return { data: product };
    } catch (error: any) {
      this.handleError(error);
    }
  }
}
