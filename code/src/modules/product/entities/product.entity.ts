import { Field, Float, ObjectType } from '@nestjs/graphql';
import { Product as PrismaProduct } from '@prisma/client';

@ObjectType()
export class Product implements PrismaProduct {
  @Field(() => [String], { nullable: true }) size: string[];
  @Field(() => Boolean, { nullable: true }) active: boolean;
  @Field(() => Boolean, { nullable: true }) published: boolean;
  @Field(() => Date, { nullable: true }) createdAt: Date;
  @Field(() => Date, { nullable: true }) updatedAt: Date;
  @Field(() => Float, { nullable: true }) discount: number;
  @Field(() => Float, { nullable: true }) rating: number;
  @Field(() => String, { nullable: true }) categoryId: string;
  @Field(() => String, { nullable: true }) description: string;
  @Field(() => String, { nullable: true }) id: string;
  @Field(() => String, { nullable: true }) image: string;
  @Field(() => String, { nullable: true }) model: string;
  @Field(() => String, { nullable: true }) slug: string;
  @Field(() => String, { nullable: true }) status: string | null;
  @Field(() => String, { nullable: true }) thumbnail: string;
  @Field(() => String, { nullable: true }) title: string;
  @Field(() => String, { nullable: true }) unit: string | null;
}
