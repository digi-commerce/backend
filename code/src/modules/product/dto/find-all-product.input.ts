import { Field, InputType, Int } from '@nestjs/graphql';
import {
  IsOptional,
  IsUUID,
  IsString,
  IsNumber,
  IsBoolean,
} from 'class-validator';
import { CreateOrderByInput } from 'src/configs/graphql/dto/order-by.input';

@InputType()
export class FindAllProductInput extends CreateOrderByInput(
  ['name', 'categoryId', 'updatedAt', 'createdAt'],
  'FindAllProductInput',
) {
  @IsOptional() @IsUUID(4) @Field(() => String, { nullable: true }) id?: string;

  @IsOptional()
  @IsString()
  @Field(() => String, { nullable: true })
  name?: string;

  @IsOptional()
  @IsString()
  @Field(() => String, { nullable: true })
  title?: string;
  @Field(() => String, { nullable: true }) categoryId?: string;

  @IsOptional()
  @IsString()
  @Field(() => String, { nullable: true })
  status?: string;

  @IsOptional()
  @IsBoolean()
  @Field(() => Boolean, { nullable: true })
  active?: boolean;

  @IsOptional()
  @IsNumber()
  @Field(() => Int, { nullable: true })
  pageSize?: number;

  @IsOptional()
  @IsNumber()
  @Field(() => Int, { nullable: true })
  pageNumber?: number;
}
