import { Field, InputType } from '@nestjs/graphql';
import { IsUUID } from 'class-validator';

@InputType()
export class FindOneProductInput {
  @IsUUID(4) @Field(() => String) id: string;
}
