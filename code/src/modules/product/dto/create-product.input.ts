import { InputType, Int, Field, Float } from '@nestjs/graphql';
import { Prisma } from '@prisma/client';
import {
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

@InputType()
export class CreateProductInput implements Prisma.ProductUncheckedCreateInput {
  @IsNotEmpty() @IsString() @Field(() => String) description: string;
  @IsNotEmpty() @IsString() @Field(() => String) model: string;
  @IsNotEmpty() @IsString() @Field(() => String) categoryId: string;
  @IsNotEmpty() @IsString() @Field(() => String) brandId: string;
  @IsOptional() @IsString() @Field(() => String) unit?: string;
  @IsNotEmpty() @IsString() @Field(() => String) slug: string;
  @IsNotEmpty() @IsString() @Field(() => String) title: string;
  @IsNotEmpty() @IsNumber() @Field(() => Float) rating: number;
  @IsNotEmpty() @IsNumber() @Field(() => Float) discount: number;
  @IsNotEmpty() @IsString() @Field(() => String) thumbnail: string;
  @IsOptional()
  @IsString({ each: true })
  @Field(() => [String])
  size?: string[];
  @IsOptional() @IsString() @Field(() => String) status?: string;
  @IsOptional()
  @IsString({ each: true })
  @Field(() => [String])
  image: string;
  @IsOptional() @IsBoolean() @Field(() => Boolean) published?: boolean;
}
