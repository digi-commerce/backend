import { ObjectType } from '@nestjs/graphql';
import OutputGeneric from 'src/configs/graphql/output-generic';
import { Product } from '../entities/product.entity';

@ObjectType()
export class ProductOutput extends OutputGeneric(Product, 'object') {}

@ObjectType()
export class ProductsOutput extends OutputGeneric(Product, 'array') {}
