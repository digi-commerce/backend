import { Field, Float, InputType } from '@nestjs/graphql';
import { Prisma } from '@prisma/client';
import { IsBoolean, IsNumber, IsOptional, IsString } from 'class-validator';

@InputType()
export class UpdateProductInput implements Prisma.ProductUpdateInput {
  @Field(() => String) id: string;
  @IsOptional() @IsString() @Field(() => String) unit?: string;
  @IsOptional() @IsString() @Field(() => String) slug?: string;
  @IsOptional() @IsString() @Field(() => String) title?: string;
  @IsOptional() @IsNumber() @Field(() => Float) rating?: number;
  @IsOptional() @IsNumber() @Field(() => Float) discount?: number;
  @IsOptional() @IsString() @Field(() => String) thumbnail?: string;
  @IsOptional()
  @IsString({ each: true })
  @Field(() => [String])
  size?: string[];
  @IsOptional() @IsString() @Field(() => String) status?: string;
  @IsOptional()
  @IsString({ each: true })
  @Field(() => [String])
  images?: string[];
  @IsOptional() @IsBoolean() @Field(() => String) published?: boolean;
}
