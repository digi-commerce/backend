import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { PrismaService } from 'src/services/prisma/prisma.service';
import { CreateProductInput } from './dto/create-product.input';
import { FindAllProductInput } from './dto/find-all-product.input';
import { UpdateProductInput } from './dto/update-product.input';

@Injectable()
export class ProductService {
  constructor(private readonly prisma: PrismaService) {}

  async create(createProductInput: CreateProductInput) {
    return await this.prisma.product.create({
      data: createProductInput,
    });
  }

  async findAll(findAllInput: FindAllProductInput) {
    const pageSize = findAllInput.pageSize ? findAllInput.pageSize : 5;
    const pageNumber = findAllInput.pageNumber ? findAllInput.pageNumber : 1;
    const offset = pageSize * (pageNumber - 1);

    const conditions: Prisma.ProductWhereInput = {
      ...(findAllInput.name && {
        name: { contains: findAllInput.name },
      }),
      ...(findAllInput.title && {
        title: { contains: findAllInput.title },
      }),
      ...(findAllInput.active && { active: findAllInput.active }),
      ...(findAllInput.status && {
        status: { contains: findAllInput.status },
      }),
      ...(findAllInput.id && { id: findAllInput.id }),
    };
    const [total, items] = (await this.prisma.$transaction([
      this.prisma.product.count({
        where: conditions,
      }),
      this.prisma.product.findMany({
        where: conditions,
        take: pageSize,
        skip: offset,
        orderBy: {
          ...(findAllInput.orderField
            ? {
                [findAllInput.orderField]: findAllInput.orderType,
              }
            : { createdAt: 'desc' }),
        },
      }),
    ])) ?? [0, []];

    const totalPage = Math.ceil(total / pageSize);

    return {
      data: items,
      pagination: { page: pageNumber, total, totalPage, pageSize },
    };
  }

  async findOne(id: string) {
    return await this.prisma.product.findUnique({ where: { id } });
  }

  async update(id: string, updateProductInput: UpdateProductInput) {
    return await this.prisma.product.update({
      where: { id },
      data: updateProductInput,
    });
  }

  async remove(id: string) {
    return await this.prisma.product.delete({ where: { id } });
  }
}
