import { NotFoundException } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { ROLE } from '@prisma/client';
import { CurrentToken } from 'src/core/decorators/CurrentTokenGql.decorator';
import { Roles } from 'src/core/decorators/roles.decorator';
import { ExceptionsHandler } from 'src/core/exceptions/exceptions-handler';
import { User } from '../users/entities/user.entity';
import { BudgetService } from './budget.service';
import {
  BudgetOutput,
  BudgetsOutput,
  ProductsValueUpdateBudgetOutput,
} from './dto/budget.output';
import { CreateBudgetInput } from './dto/create-budget.input';
import { FindAllBudgetInput } from './dto/find-all-budget.input';
import { FindOneBudgetInput } from './dto/find-one-budget.input';
import { UpdateBudgetInput } from './dto/update-budget-input';
import { ProductsValueBudgetInput } from './dto/update-values-products.input';
import { Budget } from './entities/budget.entity';

@Resolver(() => Budget)
export class BudgetResolver extends ExceptionsHandler {
  constructor(private readonly budgetService: BudgetService) {
    super();
  }

  @Roles('ORDER', 'USER')
  @Mutation(() => BudgetOutput)
  async createBudget(
    @Args('createBudgetInput') createBudgetInput: CreateBudgetInput,
  ): Promise<BudgetOutput | undefined> {
    try {
      const budget = await this.budgetService.create(createBudgetInput);
      return { data: budget };
    } catch (error) {
      this.handleError(error);
    }
  }

  @Roles('ORDER', 'USER')
  @Query(() => BudgetsOutput, { name: 'budgets' })
  async findAll(
    @Args('findAllBudgetInput') findAllBudgetInput: FindAllBudgetInput,
    @CurrentToken() user: User,
  ): Promise<BudgetsOutput | undefined> {
    try {
      if (ROLE.USER === user.role) findAllBudgetInput.userId = user.id;
      const budgets = await this.budgetService.findAll(findAllBudgetInput);
      return { data: budgets.data, meta: { pagination: budgets.pagination } };
    } catch (error) {
      this.handleError(error);
    }
  }

  @Roles('ORDER', 'USER')
  @Query(() => BudgetOutput, { name: 'budget' })
  async findOne(
    @Args('findOneBudgetInput') findOneBudgetInput: FindOneBudgetInput,
    @CurrentToken() user: User,
  ): Promise<BudgetOutput | undefined> {
    try {
      const budget = await this.budgetService.findOne(
        findOneBudgetInput.id,
        user,
      );
      if (!budget)
        throw new NotFoundException(
          `Budget ${findOneBudgetInput.id} not found`,
        );
      return { data: budget };
    } catch (error) {
      this.handleError(error);
    }
  }

  @Roles('ORDER')
  @Mutation(() => BudgetOutput)
  async valueProductsBudget(
    @Args('updateBudgetInput') updateBudgetInput: UpdateBudgetInput,
  ): Promise<BudgetOutput | undefined> {
    try {
      const budget = await this.budgetService.update(updateBudgetInput);
      if (!budget)
        throw new NotFoundException(`Budget ${updateBudgetInput.id} not found`);
      return { data: budget };
    } catch (error) {
      this.handleError(error);
    }
  }

  @Roles('ORDER', 'USER')
  @Mutation(() => BudgetOutput)
  async updateBudget(
    @Args('updateBudgetInput') updateBudgetInput: UpdateBudgetInput,
  ): Promise<BudgetOutput | undefined> {
    try {
      const budget = await this.budgetService.update(updateBudgetInput);
      if (!budget)
        throw new NotFoundException(`Budget ${updateBudgetInput.id} not found`);
      return { data: budget };
    } catch (error) {
      this.handleError(error);
    }
  }

  @Roles('ORDER')
  @Mutation(() => ProductsValueUpdateBudgetOutput)
  async productsValueBudget(
    @Args('productsValueBudgetInput')
    productsValueBudgetInput: ProductsValueBudgetInput,
  ): Promise<ProductsValueUpdateBudgetOutput | undefined> {
    try {
      const budget = await this.budgetService.productsValue(
        productsValueBudgetInput,
      );

      return { data: budget };
    } catch (error) {
      this.handleError(error);
    }
  }

  @Roles('ORDER', 'USER')
  @Mutation(() => BudgetOutput)
  async acceptOrRejectBudget(
    @Args('updateBudgetInput') updateBudgetInput: UpdateBudgetInput,
  ): Promise<BudgetOutput | undefined> {
    try {
      const budget = await this.budgetService.acceptOrRejectBudget(
        updateBudgetInput,
      );
      if (!budget)
        throw new NotFoundException(
          `Budget ${updateBudgetInput.id} not update`,
        );
      return { data: budget };
    } catch (error) {
      this.handleError(error);
    }
  }

  @Roles('ORDER')
  @Mutation(() => BudgetOutput)
  async finishOrCancelBudget(
    @Args('updateBudgetInput') updateBudgetInput: UpdateBudgetInput,
  ): Promise<BudgetOutput | undefined> {
    try {
      const budget = await this.budgetService.finishOrCancelBudget(
        updateBudgetInput,
      );
      if (!budget)
        throw new NotFoundException(
          `Budget ${updateBudgetInput.id} not update`,
        );
      return { data: budget };
    } catch (error) {
      this.handleError(error);
    }
  }
}
