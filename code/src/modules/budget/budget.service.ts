import {
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { PrismaService } from 'src/services/prisma/prisma.service';
import { FindAllBudgetInput } from './dto/find-all-budget.input';
import { UpdateBudgetInput } from './dto/update-budget-input';
import { CreateBudgetInput } from './dto/create-budget.input';
import { Prisma, ROLE, STATUS_BUDGET } from '@prisma/client';
import { User } from '../users/entities/user.entity';
import { ProductsValueBudgetInput } from './dto/update-values-products.input';

@Injectable()
export class BudgetService {
  constructor(private readonly prisma: PrismaService) {}

  async create(createBudgetInput: CreateBudgetInput) {
    return await this.prisma.budget.create({
      data: createBudgetInput,
    });
  }

  async findAll(findAllInput: FindAllBudgetInput) {
    const pageSize = findAllInput.pageSize ? findAllInput.pageSize : 5;
    const pageNumber = findAllInput.pageNumber ? findAllInput.pageNumber : 1;
    const offset = pageSize * (pageNumber - 1);

    const conditions: Prisma.BudgetWhereInput = {
      ...(findAllInput.status && {
        status: findAllInput.status,
      }),
      ...(findAllInput.id && { id: findAllInput.id }),
      ...(findAllInput.userId && { userId: findAllInput.userId }),
      ...(findAllInput.userName && {
        User: {
          firstName: { contains: findAllInput.userName },
          lastName: { contains: findAllInput.userName },
        },
      }),
    };
    const [total, items] = (await this.prisma.$transaction([
      this.prisma.budget.count({
        where: conditions,
      }),
      this.prisma.budget.findMany({
        where: conditions,
        take: pageSize,
        skip: offset,
        orderBy: {
          ...(findAllInput.orderField
            ? {
                [findAllInput.orderField]: findAllInput.orderType,
              }
            : { createdAt: 'desc' }),
        },
      }),
    ])) ?? [0, []];

    const totalPage = Math.ceil(total / pageSize);

    return {
      data: items,
      pagination: { page: pageNumber, total, totalPage, pageSize },
    };
  }

  async findOne(id: number, user: User) {
    return await this.prisma.budget.findFirst({
      where: { id, ...(user.role === ROLE.USER && { userId: user.id }) },
    });
  }

  async update(updateBudgetInput: UpdateBudgetInput) {
    return await this.prisma.budget.update({
      where: { id: updateBudgetInput.id },
      data: updateBudgetInput,
    });
  }

  async remove(id: number) {
    return await this.prisma.budget.delete({
      where: { id },
    });
  }

  async productsValue(productsValue: ProductsValueBudgetInput) {
    const budget = await this.prisma.budget.findUnique({
      where: { id: productsValue.id },
    });
    if (!budget)
      throw new NotFoundException(`No budget found for ${productsValue.id}`);

    const statusAccept: STATUS_BUDGET[] = [
      STATUS_BUDGET.NEW,
      STATUS_BUDGET.PEDDING,
      STATUS_BUDGET.IN_PROGRESS,
      STATUS_BUDGET.REJECTED,
    ];
    if (!statusAccept.includes(budget.status))
      throw new ForbiddenException(`Action not allowed for ${budget.status}`);

    const newProductsOnBudget = productsValue.productsValue.map(
      (product): Prisma.ProductOnBudgetUncheckedUpdateManyInput => {
        return {
          productId: product.productId,
          pricer: product.pricer,
          budgetId: budget.id,
        };
      },
    );
    const productOnBudgetUpdated = await this.prisma.productOnBudget.updateMany(
      {
        data: newProductsOnBudget,
      },
    );

    const budgetUpdated = await this.prisma.budget.update({
      data: { status: STATUS_BUDGET.PRICED },
      where: { id: budget.id },
    });

    return {
      totalUpdated: productOnBudgetUpdated.count,
      statusBudget: budgetUpdated.status,
    };
  }

  async acceptOrRejectBudget(updateStatus: UpdateBudgetInput) {
    if (!updateStatus.status)
      throw new NotFoundException(
        `Not status found for ${updateStatus.status}`,
      );

    if (!['ACCEPTED', 'REJECTED'].includes(updateStatus.status))
      throw new ForbiddenException(
        `Action not allowed for ${updateStatus.status}, only permissions are allowed status ${STATUS_BUDGET.ACCEPTED} and ${STATUS_BUDGET.REJECTED}`,
      );

    const budget = await this.prisma.budget.findUnique({
      where: { id: updateStatus.id },
    });
    if (!budget)
      throw new NotFoundException(`Not budget found for ${updateStatus.id}`);

    if (budget.status !== STATUS_BUDGET.PRICED)
      throw new ForbiddenException(`Action not allowed for ${budget.status}`);

    const budgetUpdated = await this.prisma.budget.update({
      data: { status: updateStatus.status },
      where: { id: budget.id },
    });

    return budgetUpdated;
  }

  async finishOrCancelBudget(updateStatus: UpdateBudgetInput) {
    if (!updateStatus.status)
      throw new NotFoundException(
        `Not status found for ${updateStatus.status}`,
      );

    const budget = await this.prisma.budget.findUnique({
      where: { id: updateStatus.id },
    });
    if (!budget)
      throw new NotFoundException(`Not budget found for ${updateStatus.id}`);

    const statusAccept: STATUS_BUDGET[] = [
      STATUS_BUDGET.REJECTED,
      STATUS_BUDGET.PEDDING,
      STATUS_BUDGET.ACCEPTED,
    ];
    if (!statusAccept.includes(budget.status))
      throw new ForbiddenException(
        `Action not allowed for ${budget.status}, only permissions are allowed status ${STATUS_BUDGET.ACCEPTED}, ${STATUS_BUDGET.REJECTED} and ${STATUS_BUDGET.PEDDING}`,
      );

    const budgetUpdated = await this.prisma.budget.update({
      data: { status: updateStatus.status },
      where: { id: budget.id },
    });

    return budgetUpdated;
  }
}
