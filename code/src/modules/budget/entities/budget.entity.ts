import {
  Field,
  Float,
  Int,
  ObjectType,
  registerEnumType,
} from '@nestjs/graphql';
import { Budget as PrismaBudget, STATUS_BUDGET } from '@prisma/client';

registerEnumType(STATUS_BUDGET, { name: 'StatusBudget' });

@ObjectType()
export class Budget implements PrismaBudget {
  @Field(() => Date, { nullable: true }) createdAt: Date;
  @Field(() => Date, { nullable: true }) updatedAt: Date;
  @Field(() => Date, { nullable: true }) validateBudget: Date;
  @Field(() => Float, { nullable: true }) discount: number;
  @Field(() => Float, { nullable: true }) grossValue: number;
  @Field(() => Float, { nullable: true }) netValue: number;
  @Field(() => Int, { nullable: true }) id: number;
  @Field(() => STATUS_BUDGET, { nullable: true }) status: STATUS_BUDGET;
  @Field(() => String, { nullable: true }) observation: string | null;
  @Field(() => String, { nullable: true }) userId: string;
}
