import { Field, Int, ObjectType } from '@nestjs/graphql';
import { STATUS_BUDGET } from '@prisma/client';

@ObjectType()
export class ProductsValueBudgetOutput {
  @Field(() => Int)
  totalUpdated: number;

  @Field(() => STATUS_BUDGET)
  statusBudget: STATUS_BUDGET;
}
