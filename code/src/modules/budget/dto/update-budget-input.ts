import { Field, InputType, Int } from '@nestjs/graphql';
import { Prisma, STATUS_BUDGET } from '@prisma/client';
import {
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';

@InputType()
export class UpdateBudgetInput implements Prisma.BudgetUncheckedUpdateInput {
  @IsNotEmpty()
  @IsInt()
  @Field(() => Int)
  id: number;

  @IsOptional()
  @IsString()
  @Field(() => String, { nullable: true })
  observation?: string;

  @IsOptional()
  @IsEnum(STATUS_BUDGET)
  @Field(() => STATUS_BUDGET, { nullable: true })
  status?: STATUS_BUDGET;
}
