import { Field, Float, InputType, Int } from '@nestjs/graphql';
import { Prisma } from '@prisma/client';
import { Type } from 'class-transformer';
import {
  IsDate,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsUUID,
  Min,
  ValidateNested,
} from 'class-validator';

@InputType()
export class CreateBudgetInput implements Prisma.BudgetUncheckedCreateInput {
  @IsNotEmpty() @IsUUID(4) @Field(() => String) userId: string;
  @IsOptional() @Field(() => String) observation?: string;
  @IsNotEmpty() @IsDate() @Field(() => Date) validateBudget: Date;

  @IsNotEmpty()
  @Type(() => CreateProductOnBudgetInput)
  @ValidateNested({ each: true })
  @Field(() => [CreateProductOnBudgetInput])
  products: CreateProductOnBudgetInput[];
}

@InputType()
export class CreateProductOnBudgetInput
  implements Prisma.ProductOnBudgetUncheckedCreateWithoutBudgetInput
{
  @IsNotEmpty() @IsUUID(4) @Field(() => String) productId: string;
  @IsOptional() @Field(() => Float) pricer?: number;
  @IsNotEmpty() @Min(1) @IsInt() @Field(() => Int) qntd: number;
}
