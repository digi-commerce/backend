import { Field, InputType, Int } from '@nestjs/graphql';
import { STATUS_BUDGET } from '@prisma/client';
import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { CreateOrderByInput } from 'src/configs/graphql/dto/order-by.input';

@InputType()
export class FindAllBudgetInput extends CreateOrderByInput(
  ['id', 'updatedAt', 'createdAt', 'grossValue', 'netValue'],
  'FindAllBudgetInput',
) {
  @IsOptional()
  @IsNumber()
  @Field(() => Int, { nullable: true })
  id?: number;

  @IsOptional()
  @IsEnum(STATUS_BUDGET)
  @Field(() => STATUS_BUDGET, { nullable: true })
  status?: STATUS_BUDGET;

  @IsOptional()
  @IsNumber()
  @Field(() => Int, { nullable: true })
  pageSize?: number;

  @IsOptional()
  @IsNumber()
  @Field(() => Int, { nullable: true })
  pageNumber?: number;

  @IsOptional()
  @IsString()
  @Field(() => String, { nullable: true })
  userName?: string;

  @IsOptional()
  @IsUUID(4)
  @Field(() => String, { nullable: true })
  userId?: string;
}
