import { ObjectType } from '@nestjs/graphql';
import OutputGeneric from 'src/configs/graphql/output-generic';
import { Budget } from '../entities/budget.entity';
import { ProductsValueBudgetOutput } from './update-values-products.output';

@ObjectType()
export class BudgetOutput extends OutputGeneric(Budget, 'object') {}

@ObjectType()
export class BudgetsOutput extends OutputGeneric(Budget, 'array') {}

@ObjectType()
export class ProductsValueUpdateBudgetOutput extends OutputGeneric(
  ProductsValueBudgetOutput,
  'object',
) {}
