import { Field, Float, InputType, Int } from '@nestjs/graphql';
import { Prisma } from '@prisma/client';
import { Type } from 'class-transformer';
import {
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  IsUUID,
  ValidateNested,
} from 'class-validator';

@InputType()
export class UpdateProductOnBudgetInput {
  @IsNotEmpty() @IsUUID(4) @Field(() => String) productId: string;
  @IsNotEmpty() @IsNumber() @Field(() => Float) pricer?: number;
}

@InputType()
export class ProductsValueBudgetInput
  implements Prisma.BudgetUncheckedUpdateInput
{
  @IsNotEmpty()
  @IsInt()
  @Field(() => Int)
  id: number;

  @IsNotEmpty()
  @IsString()
  @Field(() => String)
  observation?: string;

  @IsOptional()
  @IsNumber()
  @Field(() => Float)
  discount?: number;

  @Type(() => UpdateProductOnBudgetInput)
  @ValidateNested({ each: true })
  @Field(() => [UpdateProductOnBudgetInput])
  productsValue: UpdateProductOnBudgetInput[];
}
