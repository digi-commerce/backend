import { Prisma } from '@prisma/client';
import { genPassword } from 'src/core/utils/bcrypt';

export const users: Prisma.UserCreateInput[] = [
  {
    email: 'admin@digicommerce.com.br',
    firstName: 'Admins',
    password: genPassword('Digi@20'),
    role: 'MANAGER',
  },
  {
    email: 'user@digicommerce.com.br',
    firstName: 'User TESTE',
    password: genPassword('Digi@20'),
    role: 'USER',
  },
];
