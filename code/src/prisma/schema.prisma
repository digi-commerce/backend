generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_URL")
}

enum ROLE {
  MANAGER
  ORDER
  USER
}

model User {
  id        String   @id @default(dbgenerated("gen_random_uuid()")) @db.Uuid
  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @default(now()) @updatedAt @map("updated_at")

  firstName       String    @db.VarChar(20)
  lastName        String?   @db.VarChar(80)
  email           String    @unique @db.VarChar(80)
  phone           String?
  avatar          String?
  document        String?
  loginWithGoogle Boolean   @default(false) @map("login_with_google")
  password        String    @db.VarChar(80)
  dateOfBirth     DateTime?
  active          Boolean   @default(true)
  role            ROLE      @default(USER)

  confirmEmail  String? @map("confirm_email")
  resetPassword String? @map("reset_password")

  Address Address?
  Budget  Budget[]

  @@map("users")
}

model Address {
  id        String   @id @default(dbgenerated("gen_random_uuid()")) @db.Uuid
  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @default(now()) @updatedAt @map("updated_at")

  title        String
  cep          String
  number       Int
  complement   String?
  address      String
  neighborhood String
  cityId       Int     @map("city_id")
  userId       String  @unique @map("user_id") @db.Uuid

  City City @relation(fields: [cityId], references: [id], onDelete: NoAction)
  User User @relation(fields: [userId], references: [id], onDelete: Cascade)

  @@map("addresses")
}

model State {
  id        Int      @id @default(autoincrement())
  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @default(now()) @updatedAt @map("updated_at")

  name String
  uf   String @db.VarChar(2)

  Cities City[]

  @@map("states")
}

model City {
  id        Int      @id @default(autoincrement())
  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @default(now()) @updatedAt @map("updated_at")

  name     String
  stateId  Int    @map("state_id")
  ibgeCode Int    @map("ibge_code")

  State     State     @relation(fields: [stateId], references: [id], onDelete: Cascade)
  Addresses Address[]

  @@map("cities")
}

model Product {
  id        String   @id @default(dbgenerated("gen_random_uuid()")) @db.Uuid
  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @default(now()) @updatedAt @map("updated_at")

  unit        String?
  slug        String
  title       String
  rating      Float
  discount    Float    @default(0)
  thumbnail   String
  size        String[]
  status      String?
  image       String
  description String
  published   Boolean  @default(false)
  active      Boolean  @default(true)
  model       String
  categoryId  String   @map("category_id") @db.Uuid

  Category        Category          @relation(fields: [categoryId], references: [id], onDelete: Cascade)
  ProductOnBudget ProductOnBudget[]

  @@map("products")
}

model Category {
  id        String   @id @default(dbgenerated("gen_random_uuid()")) @db.Uuid
  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @default(now()) @updatedAt @map("updated_at")
  name      String

  Products Product[]

  @@map("categories")
}

model ProductOnBudget {
  budgetId  Int      @map("budget_id")
  productId String   @map("product_id") @db.Uuid
  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @default(now()) @updatedAt @map("updated_at")

  pricer Float @default(0)
  qntd   Float

  Budget  Budget  @relation(fields: [budgetId], references: [id], onDelete: Cascade)
  Product Product @relation(fields: [productId], references: [id], onDelete: NoAction)

  @@id([budgetId, productId])
  @@map("product_on_budgets")
}

enum STATUS_BUDGET {
  NEW
  PEDDING
  DONE
  CANCELED
  IN_PROGRESS
  PRICED
  REJECTED
  ACCEPTED
}

model Budget {
  id        Int      @id @default(autoincrement())
  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @default(now()) @updatedAt @map("updated_at")

  userId         String        @map("user_id") @db.Uuid
  observation    String?
  discount       Float         @default(0)
  validateBudget DateTime      @map("validate_budget")
  grossValue     Float         @default(0) @map("gross_value")
  netValue       Float         @default(0) @map("net_value")
  status         STATUS_BUDGET @default(NEW)

  User             User              @relation(fields: [userId], references: [id], onDelete: NoAction)
  ProductOnBudgets ProductOnBudget[]

  @@map("budgets")
}
