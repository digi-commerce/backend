/*
  Warnings:

  - The values [ADMIN] on the enum `ROLE` will be removed. If these variants are still used in the database, this will fail.
  - You are about to drop the column `brand_id` on the `products` table. All the data in the column will be lost.
  - Added the required column `description` to the `products` table without a default value. This is not possible if the table is not empty.
  - Made the column `published` on table `products` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterEnum
BEGIN;
CREATE TYPE "ROLE_new" AS ENUM ('MANAGER', 'ORDER', 'USER');
ALTER TABLE "users" ALTER COLUMN "role" DROP DEFAULT;
ALTER TABLE "users" ALTER COLUMN "role" TYPE "ROLE_new" USING ("role"::text::"ROLE_new");
ALTER TYPE "ROLE" RENAME TO "ROLE_old";
ALTER TYPE "ROLE_new" RENAME TO "ROLE";
DROP TYPE "ROLE_old";
ALTER TABLE "users" ALTER COLUMN "role" SET DEFAULT 'USER';
COMMIT;

-- AlterTable
ALTER TABLE "products" DROP COLUMN "brand_id",
ADD COLUMN     "description" TEXT NOT NULL,
ALTER COLUMN "published" SET NOT NULL,
ALTER COLUMN "published" SET DEFAULT false;

-- AlterTable
ALTER TABLE "users" ADD COLUMN     "login_with_google" BOOLEAN NOT NULL DEFAULT false;
