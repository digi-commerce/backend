export PROJECT_NAME := digicommerce
export CONTAINER_NAME := api-digicommerce
export CONTAINER_PROD_NAME := ${CONTAINER_NAME}-prod
export COMPOSE_COMMAND        := docker-compose 
export COMPOSE_PROJECT        := ${COMPOSE_COMMAND} --project-name ${PROJECT_NAME}
export COMPOSE_IGNORE_ORPHANS := true
export IMAGE_NAME := ${PROJECT_NAME}-${CONTAINER_NAME}
export IMAGE_PROD_NAME := ${PROJECT_NAME}-${COMPOSE_PROD_NAME}

all: dev
attach:; docker exec -it ${CONTAINER_NAME} sh
clean:;  rm -f -r "code/build" "code/dist" "code/node_modules"
dev:;    ${COMPOSE_PROJECT} --file docker-compose.dev.yml up --build
logs:;   docker logs ${CONTAINER_NAME}
prod:;   ${COMPOSE_PROJECT} up
rm:;     docker rm -f ${CONTAINER_PROD_NAME} ${CONTAINER_NAME}
rmi:;    docker rmi -f ${IMAGE_PROD_NAME} ${IMAGE_NAME}
rmiv:;   ${COMPOSE_PROJECT} down --rmi local --volumes
start:;  ${COMPOSE_PROJECT} $(@)
stop:;   ${COMPOSE_PROJECT} $(@)
up:;     ${COMPOSE_PROJECT} up --detach
rmall:; rm rmi
rmp:; rm rmi prod
res:; rmall dev

.PHONY: all
.PHONY: clean
.PHONY: dev
.PHONY: logs
.PHONY: prod
.PHONY: rm
.PHONY: rmi
.PHONY: rmiv
.PHONY: start
.PHONY: stop
.PHONY: up
.PHONY: rmall
.PHONY: rmp
.PHONY: res